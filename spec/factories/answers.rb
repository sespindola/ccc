FactoryGirl.define do
  factory :answer do
    challenge nil
    user nil
    status "MyText"
    score_discount 1
    validation_hash "MyText"
  end
end
