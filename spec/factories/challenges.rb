FactoryGirl.define do
  factory :challenge do
    name "MyText"
    description "MyText"
    score 1
    solution_hash 1
    hints "MyText"
  end
end
