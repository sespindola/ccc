Rails.application.routes.draw do
  resources :challenges, only: [:index , :show]
  devise_for :users
end
