class CreateChallenges < ActiveRecord::Migration[5.1]
  def change
    create_table :challenges do |t|
      t.text :name
      t.text :description
      t.integer :score
      t.integer :solution_hash
      t.text :hints

      t.timestamps
    end
  end
end
