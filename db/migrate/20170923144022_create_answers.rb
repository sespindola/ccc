class CreateAnswers < ActiveRecord::Migration[5.1]
  def change
    create_table :answers do |t|
      t.references :challenge, foreign_key: true
      t.references :user, foreign_key: true
      t.text :status
      t.integer :score_discount
      t.text :validation_hash

      t.timestamps
    end
  end
end
